TERMUX_PKG_HOMEPAGE=https://metacpan.org/pod/Pod::Parser
TERMUX_PKG_DESCRIPTION="Base class for creating POD filters and translators"
TERMUX_PKG_LICENSE="GPL-2.0"
TERMUX_PKG_MAINTAINER="Peter Horvath horvath.akos.peter@gmail.com"
TERMUX_PKG_VERSION=1.66
TERMUX_PKG_REVISION=5
TERMUX_PKG_SRCURL=http://deb.debian.org/debian/pool/main/libp/libpod-parser-perl/libpod-parser-perl_${TERMUX_PKG_VERSION}.orig.tar.gz
TERMUX_PKG_SHA256=22928a7bffe61b452c05bbbb8f5216d4b9cf9fe2a849b776c25500d24d20df7c
TERMUX_PKG_DEPENDS="perl"
TERMUX_PKG_BUILD_IN_SRC=true
TERMUX_PKG_PLATFORM_INDEPENDENT=true

termux_step_configure() {
  perl Build.PL
}

termux_step_make_install() {
  local perl_version=$(. $TERMUX_SCRIPTDIR/packages/perl/build.sh; echo $TERMUX_PKG_VERSION)

  # mkdir -p $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/share/man/man3/
  # cp $TERMUX_PKG_SRCDIR/blib/man3/Regexp::Assemble.3pm \
  #    $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/share/man/man3/

  # mkdir -p $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Regexp/
  # cp $TERMUX_PKG_SRCDIR/lib/Regexp/Assemble.pm \
  #    $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Regexp/

  mkdir -pv $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
  cp -v $TERMUX_PKG_SRCDIR/lib/Module/Build.pm $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
  cp -vr $TERMUX_PKG_SRCDIR/lib/Module/Build/ $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
}
