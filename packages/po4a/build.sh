TERMUX_PKG_HOMEPAGE=https://packages.debian.org/sid/po4a
TERMUX_PKG_DESCRIPTION="tools to ease the translation of documentation"
TERMUX_PKG_LICENSE="GPL-2.0"
TERMUX_PKG_MAINTAINER="Peter Horvath horvath.akos.peter@gmail.com"
TERMUX_PKG_VERSION=0.69-1
TERMUX_PKG_SRCURL=http://deb.debian.org/debian/pool/main/p/po4a/po4a_0.69.orig.tar.gz
TERMUX_PKG_SHA256="d578df98afaffa847fb4b73216cf761df4fdd669b617783ca7f45f4172c0e212"
TERMUX_PKG_BUILD_IN_SRC=true
