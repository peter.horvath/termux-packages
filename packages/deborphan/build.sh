TERMUX_PKG_HOMEPAGE=https://packages.debian.org/source/sid/deborphan
TERMUX_PKG_DESCRIPTION="Program that can find unused packages"
TERMUX_PKG_LICENSE="MIT"
TERMUX_PKG_MAINTAINER="Pierre Rudloff @Rudloff"
TERMUX_PKG_VERSION=1.7.35
TERMUX_PKG_SRCURL=http://deb.debian.org/debian/pool/main/d/deborphan/deborphan_1.7.35.tar.xz
TERMUX_PKG_SHA256="5dddd805a4576556385b3f002cb220bbca7365768bc170b9e39528f2cc47974d"
TERMUX_PKG_BUILD_IN_SRC=true
