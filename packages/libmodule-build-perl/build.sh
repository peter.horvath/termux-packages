TERMUX_PKG_HOMEPAGE=https://metacpan.org/dist/Module-Build
TERMUX_PKG_DESCRIPTION="Framework for building and installing Perl modules"
TERMUX_PKG_LICENSE="GPL-2.0"
TERMUX_PKG_MAINTAINER="Peter Horvath horvath.akos.peter@gmail.com"
TERMUX_PKG_VERSION=0.423400
TERMUX_PKG_REVISION=5
TERMUX_PKG_SRCURL=http://deb.debian.org/debian/pool/main/libm/libmodule-build-perl/libmodule-build-perl_${TERMUX_PKG_VERSION}.orig.tar.gz
TERMUX_PKG_SHA256=66aeac6127418be5e471ead3744648c766bd01482825c5b66652675f2bc86a8f
TERMUX_PKG_DEPENDS="perl"
TERMUX_PKG_BUILD_IN_SRC=true
TERMUX_PKG_PLATFORM_INDEPENDENT=true

termux_step_configure() {
  perl Build.PL
}

termux_step_make_install() {
  local perl_version=$(. $TERMUX_SCRIPTDIR/packages/perl/build.sh; echo $TERMUX_PKG_VERSION)

  # mkdir -p $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/share/man/man3/
  # cp $TERMUX_PKG_SRCDIR/blib/man3/Regexp::Assemble.3pm \
  #    $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/share/man/man3/

  # mkdir -p $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Regexp/
  # cp $TERMUX_PKG_SRCDIR/lib/Regexp/Assemble.pm \
  #    $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Regexp/

  mkdir -pv $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
  cp -v $TERMUX_PKG_SRCDIR/lib/Module/Build.pm $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
  cp -vr $TERMUX_PKG_SRCDIR/lib/Module/Build/ $TERMUX_PKG_MASSAGEDIR/$TERMUX_PREFIX/lib/perl5/site_perl/$perl_version/Module/
}
